package ch.model;

import java.util.List;

interface GroupInterface {

    void setNilaiLebihDari(List<Integer> data, int delimiter);

    void setNilaiKurangDari(List<Integer> data, int delimiter);

    void setNilaiTengah(List<Integer> data, int delimiter);

}
