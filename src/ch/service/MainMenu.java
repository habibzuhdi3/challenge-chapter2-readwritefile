package ch.service;

import java.util.Scanner;

/**
 * Implementasi Inheritance dari MainMenuAbstract Class
 */
public class MainMenu extends MainMenuAbstract {

    /**
     * Polymorhfing tampil method(Overriding)
     */
    public void tampil() {
        Scanner input = new Scanner(System.in);
        System.out.println("============================");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("============================");

        System.out.println("Letakkan file data_sekolah.csv difolder yang sama dengan aplikasi ini.");
        System.out.println("Jika sudah, tekan ENTER");
        input.nextLine();
        ReadWrite readWrite = new ReadWrite();

        System.out.println("Output akan di generate ke folder \"Output\" yang baru saja dibuat");
        System.out.println("1. Generate File untuk menampilkan mean, median, dan modus");
        System.out.println("2. Generate File untuk menampilkan pengelmompokan data");
        System.out.println("3. Generate kedua file");
        System.out.println("0. Exit");
        System.out.print("Silahkan pilih menu: ");
        int pilih = input.nextInt();
        NextMenu next = new NextMenu();
        switch (pilih) {
            case 1 -> {
                readWrite.generateOperation("MeanMedianModus");
                System.out.println("File succesfully written");
                next.tampil();
            }
            case 2 -> {
                System.out.println("Masukkan Nilai Pembatas : ");
                int pembatas = input.nextInt();
                readWrite.generateGroup("Grouping", pembatas);
                System.out.println("File succesfully written");
                next.tampil();
            }
            case 3 -> {
                System.out.println("Masukkan Nilai Pembatas : ");
                int pembatas = input.nextInt();
                readWrite.generateOperation("MeanMedianModus");
                readWrite.generateGroup("Grouping", pembatas);
                System.out.println("File succesfully written");
                next.tampil();
            }
            case 0 -> {
                System.out.println("Program SELESAI");
                System.exit(0);
            }
            default -> {
                System.out.println("Pilihan tidak ditemukan, silahkan pilih lagi");
                this.tampil();
            }
        }
    }
}
